# Photogalleries

**Сайт для бесплатного просмотра и скачивание фото, c использование unsplash api**

---
## Основные технологий шаблона:
- [Capacitor](https://capacitorjs.com/) - инструмент для сборки гибридного-мобильного приложения
- [Nuxt.js](https://nuxtjs.org/) - фреймворк над Vue.js, для server-side rendering
- [Express.js](https://expressjs.com/ru/) – backend фреймворк используемый в данном проекте как serverMiddleware
- [JSDoc](https://jsdoc.app/) / [JSDoc-VueJS](https://github.com/Kocal/jsdoc-vuejs#readme) - движок докумментирования проекта
---
## Основные команды
- Для запуска проекта на локальном сервере в режиме разработке
>>```
>>$ npm run serve или $ yarn serve
>>```

- Для запуска продакш версий веб-приложения
>>```
>>$ npm run serve:prod или $ yarn serve:prod
>>```

- Для запуска документации версий веб-приложения
>>```
>>$ npm run serve:docs или $ yarn serve:docs
>>```

- Для сборки проекта
>>```
>>$ npm run build или $ yarn build
>>```

- Для сборки документации
>>```
>>$ npm run build:docs или $ yarn build:docs
>>```

- Для сборки статичной версий веб-приложения
>>```
>>$ npm run generate или $ yarn generate
>>```

- Для запуска мобильной версий приложения в XCode 
>>```
>>$ npm run run:ios или $ yarn run:ios
>>```

- Для запуска мобильной версий приложения в Android Studio 
>>```
>>$ npm run run:android или $ yarn run:android
>>```

- Для копирования статичной сборки для мобильной-версий приложения
>>```
>>$ npm run copy или $ yarn copy
>>```
---
## CLI команды 

- Для создания компонента
>>```
>>$ npm run create-component или $ yarn create-component
>>```
- Для создания страницы
>>```
>>$ npm run create-page или $ yarn create-page
>>```
- Для создания модуля store
>>```
>>$ npm run create-store или $ yarn create-store
>>```
---
## Структура компонентов

Для структурирования компонентов используется методология Atomic Design. Компоненты деляться на:
- /atoms/* – тут расположены примитивные компоненты: кнопки, заголовки, поля ввода и пр.
- /moleculus/* – тут расположены связки/группы примитивов: группы кнопок, карточки и пр.
- /organisms/* – тут расположены связки/группы молекул. То есть компоненты, которая состоит из молекул, которые состоят из примитивов: шапка, списки карточек, модальные окна и пр.
- /templates/* – тут расположены шаблоны страницы

[Подробнее...](/src/components/README.md)

Подробнее можно почитать:
- [Atomic Design Methodology](https://atomicdesign.bradfrost.com/chapter-2/)
- [Atomic Design in practice](https://blog.ippon.tech/atomic-design-in-practice/)
---
## Сервер (Server Side Rendering)

В проекте используется бэкенд фреймворк для ssr - [express.js](https://expressjs.com/ru/)

Стоит ознакомится с документацией Nuxt о [serverMiddleware](https://ru.nuxtjs.org/api/configuration-servermiddleware/) и с самим [express.js](https://expressjs.com/ru/) для работы с серверной частью приложения в ``` universal ``` режиме.


---

## Правила документирования проекта
Для документирования проекта используется [JSDoc](https://jsdoc.app/) и плагин [JSDoc-VueJS](https://github.com/Kocal/jsdoc-vuejs#readme) для поддержки VueJS компонентов.

Компонент – модуль, соответсвенно надо к каждому компоненту добавлять ключ ``` /** @module */ ```

Каждому компоненту надо задавать имя, и добавлять ключ ``` /** @name */ ``` и описывать компонент

Желательно так же оставлять пример использования метода или самого компонента при помощи ключа ``` /** @example */ ```

Также, надо указывать какие параметры имеет метод и какой тип возвращает.

Синтаксис:
```javascript
<script>
/**
 * @module Button
 * @vue-prop {String} type – Button type, values – 'accent', 'secondary', 'other'
 * @vue-prop {String} text – Text content of button
 * @vue-prop {Function} onClick - Function which fires on button click event
 */
export default {
  /**
   * @name Button
   * @desc
   * A component that renders single button.
	 * This button has 3 types – `'accent'`, `'secondary'`, `'other'`
   * @example
   * <Button text="Submit" type="accent" @click="() => console.log('hi')" />
   */
  name: 'Button',
  props: {
    type: {
      type: String,
      default: 'accent'
    },
    text: {
      type: String,
      required: true
    },
    onClick: {
      type: (e /** MouseEvent<Button> */) => {},
      required: true
    }
  },
  methods: {
    /**
     * Method that handles click event of button
     * @param {MouseEvent<Button>} e - MouseEvent object
    */
    clickHandler(e) {
      this.onClick(e);
    }
  }
}
</script>
```
---
