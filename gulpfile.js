const gulp = require('gulp');
const browserSync = require('browser-sync');
const fs = require('fs');

function LocalServerDocs() {
	const nameVersion = JSON.parse(
		fs.readFileSync('package.json', 'utf-8', err => {
			if (err) {
				return console.error('Something went wrong -> ', err);
			}
		})
	);
	browserSync({
		notify: false,
		port: 9000,
		server: {
			baseDir: [`docs/${nameVersion.name}/${nameVersion.version}`]
		}
	});
}

gulp.task('serve:docs', gulp.series(LocalServerDocs));