import nodeFetch from 'node-fetch'
import { createApi } from 'unsplash-js'

const unsplash = createApi({
  accessKey: 'HPjatvq0gH5Oz4fJltBLS17t9jPv2_MTurmTcwpcZgg',
  fetch: nodeFetch,
})

export const state = () => ({
  photos: [],
  favorites: [],
  tags: [],
})

export const mutations = {
  fetchPhotos(state, photos) {
    state.photos = [...state.photos, ...photos]
  },
  addToFavorites(state, photo) {
    state.favorites = [...state.favorites, photo]
  },
  removeFromFavorites(state, photoId) {
    state.favorites = state.favorites.filter((item) => item.id !== photoId)
  },
  fetchTags(state, tags) {
    state.tags = tags
  },
}

export const actions = {
  async fetchPhotos({ commit }, { page }) {
    try {
      const res = await unsplash.photos.list({ page, perPage: 10 })
      commit('fetchPhotos', res.response.results)
    } catch (error) {
      console.log(error.message)
    }
  },
  addOrRemoveFavorites({ commit, state }, photo) {
    if (state.favorites.some((favorite) => favorite.id === photo.id)) {
      commit('removeFromFavorites', photo.id)
    } else {
      commit('addToFavorites', photo)
    }
  },
  async fetchTags({ commit }) {
    try {
      const res = await unsplash.topics.list({ perPage: 22 })
      commit('fetchTags', res.response.results)
    } catch (error) {
      console.log(error.message)
    }
  },
}

export const getters = {
  getPhotos: (state) => state.photos,
  getFavorites: (state) => state.favorites,
  getTags: (state) => state.tags,
}
